//
//  DetailViewController.swift
//  HelloSingleView
//
//  Created by William Söder on 2019-10-25.
//  Copyright © 2019 William Söder. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
    @IBAction func btnClick(_ sender: Any) {
        print("Hello button")
    }
}

